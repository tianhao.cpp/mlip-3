#/bin/bash

# Preamble, common for all examples
MLP_EXE=../../../bin/mlp
TMP_DIR=./out
mkdir -p $TMP_DIR

# Body:
$MLP_EXE calculate_efs pot.almtp train.cfg --output_filename=$TMP_DIR/calculated.cfg

